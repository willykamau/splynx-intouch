<?php

namespace app\controllers;

use app\models\PayInvoice;
use app\models\Customer;
use app\models\IntouchPaymentApi;
use app\models\PayInvoiceIntouchAPI;
use splynx\models\finance\Invoices;
use splynx\models\finance\payments\BasePaymentInvoice;
use splynx\models\customer\BaseCustomer;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use yii;
use yii\web\Controller;
use yii\web\Response;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($item_id)
    {
        $this->getView()->title = 'Splynx Touchpay Payment';

        //find invoice
        $invoice = Invoices::findByNumber($item_id);
        $customer_id = Yii::$app->getSession()->get('splynx_customer_id', null);

        if (empty($invoice)) {
            throw new BadRequestHttpException('Invoice #' . $item_id . 'is not found.');
        }

        if ($invoice->status == Invoices::STATUS_PAID) {
            throw new BadRequestHttpException('Invoice #'  .$item_id.  'is already Paid!');
        }

        $model = new PayInvoice();



        $model->invoice_number = (int)$item_id;
        $model->amount = floatval($invoice->total);
        $model->status = ($invoice->status);
        $model->number = ($invoice->number);
        $model->customer_id = ($invoice->customer_id);


        $customer = (new BaseCustomer())->findById($customer_id);

        // invoice payment
        if (Yii::$app->request->isPost)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load(Yii::$app->request->post())) 
            {
                $model->initiatePayment();
            }    

        } else
        {       
        return $this->render('index', [
            'model' => $model,
            'invoice' => $invoice,
            'customer' => $customer,
        ]);
        }

    }
    

}


