Splynx Add-on Touchpay
======================

change splynx-addon-skeleton -> splynx-intouch

Splynx Add-on Skeleton based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.4"
composer install
~~~

Install Splynx Add-on Touchpay:
~~~
cd /var/www/splynx/addons/
git clone https://willykamau@bitbucket.org/willykamau/splynx-intouch.git
cd splynx-intouch
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-intouch/web/ /var/www/splynx/web/intouch
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-intouch.addons
~~~

with following content:
~~~
location /intouch
{
        try_files $uri $uri/ /intouch/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

