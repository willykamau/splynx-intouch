<html>
<head>
<title> Touchpay Payment Gateway</title>
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<form class="form-horizontal" action="init.php" method="POST">
  <fieldset>
    <div id="legend">
      <legend class="">Touchpay Simulate Payments</legend>
    </div>

    <!-- Splynx Customer Fisrtname -->
    <div class="control-group">
      <label class="control-label"  for="firstname">Fisrtname</label>
      <div class="controls">
        <input type="text" id="fisrtname" name="firstname" placeholder="" class="input-xlarge">
        <p class="help-block">Splynx Customer Fisrtname</p>
      </div>
    </div>
    
    <!-- Splynx Customer Lastname -->
    <div class="control-group">
      <label class="control-label"  for="lastname">Lastname</label>
      <div class="controls">
        <input type="text" id="lastname" name="lastname" placeholder="" class="input-xlarge">
        <p class="help-block">Splynx Customer Lastname</p>
      </div>
    </div>

    <!-- E-mail -->
    <div class="control-group">
      <label class="control-label" for="email">E-mail</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="" class="input-xlarge">
        <p class="help-block">Splynx Customer E-mail</p>
      </div>
    </div>

    <!-- Splynx customer phone number -->
    <div class="control-group">
        <label class="control-label" for="phone">Phone</label>
        <div class="controls">
            <input type="text" id="phone" name="phone" placeholder="" class="input-xlarge">
            <p class="help-block">Customer phone number - Without country code e.g 791793347</p>
        </div>
    </div>

    <!-- Splynx Invoice Amount -->
    <div class="control-group">
        <label class="control-label" for="phone">Amount</label>
        <div class="controls">
            <input type="text" id="amount" name="amount" placeholder="" class="input-xlarge">
            <p class="help-block">Splynx Invoice - Amount to be paid</p>
        </div>
    </div>

    <!--Transaction Type depending on Country-->
    <div class="control-group">
        <label class="control-label" for="servicecode">Service Code</label>
            <div class="controls">
                <select name="servicecode" class="input-xlarge">
                <option value="PAIEMENTMARCHANDOM">ORANGE MONEY - SENEGAL</option>
                <option value="KN_CASHOUTPM_SAFARICOM">LIPA NA MPESA - KENYA</option>
                </select>
            </div>
    </div>

    <!-- Pay Button -->
    <div class="control-group">
      <div class="controls">
        <button class="btn btn-success" type="submit">Send Request</button>
      </div>
    </div>
  </fieldset>
</form>
</body>
</html>


