<?php
$splynxMysqlConfig = \splynx\helpers\ConfigHelper::getSplynxConfig('mysql');
$splynxDbConfig = $splynxMysqlConfig['db'];

if (empty($splynxDbConfig)) {
    return [
        'class' => 'yii\db\Connection',
        'dsn' => 'sqlite:@app/data/data.db',
        'charset' => 'utf8'
    ];
}

$password = exec('/var/www/splynx/system/script/security decrypt --hash="' . $splynxDbConfig['password'] . '"');
$tablePrefix = 'INTOUCH_';

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $splynxDbConfig['host'] . ';dbname=' . $splynxDbConfig['database'],
    'username' => $splynxDbConfig['username'],
    'password' => $password,
    'charset' => $splynxDbConfig['charset'],
    'tablePrefix' => 'INTOUCH_',
];
