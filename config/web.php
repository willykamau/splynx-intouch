<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/touchpay',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Ftouchpay',
                'enableAutoLogin' => false,
            ],
         'db' => new \yii\helpers\ReplaceArrayValue(require(__DIR__ . '/db.php')),
        ],
    ];
};
