<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'consoleRunner' => [
                'class' => 'vova07\console\ConsoleRunner',
                'file' => $baseDir . '/yii'
            ],
            'db' => new \yii\helpers\ReplaceArrayValue(require(__DIR__ . '/db.php')),
        ],
    ];
};
