<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public $module_status = self::MODULE_STATUS_ENABLED;

    public function getAddOnTitle()
    {
        return 'Splynx Intouch Module';
    }

    public function getModuleName()
    {
        return 'splynx_intouch';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
            [
                'controller' => 'api\admin\finance\BankStatements',
                'action' => ['index', 'view', 'add'],
            ],
            [
                'controller' => 'api\admin\finance\BankStatementRecords',
                'action' => ['index', 'view', 'add', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Payments',
                'action' => ['index', 'add', 'delete'],
            ],
            [
                'controller' => 'api\admin\finance\Transaction',
                'action' => ['index', 'add', 'delete'],
            ],
            [
                'controller' => 'api\admin\finance\Invoices',
                'action' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Requests',
                'action' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerPayment\Accounts',
                'action' => ['index', 'view', 'update', 'delete'],
            ],
        ];
    }

    public function getEntryPoints()
    {
        
        return [
        // entrypoint payinvoice    
            [
                'name' => 'intouch',
                'title' => 'Pay via Touchpay',
                'root' => 'controllers\admin\CustomersController',
                'place' => 'portal',
                'model' => 'Invoices',
                'type' => 'action_link',
                'url' => '%2Fintouch',
                'icon' => 'fa-credit-card',
            ],
        ];
    }
}
