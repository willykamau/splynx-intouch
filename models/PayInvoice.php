<?php

namespace app\models;

use splynx\models\finance\payments;
use splynx\models\finance\payments\BasePaymentInvoice;
use splynx\models\finance\Invoices;
use splynx\models\finance\BaseInvoice;
use splynx\models\customer\BaseCustomer;
use yii\helpers\ArrayHelper;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Model;

class PayInvoice extends ActiveRecord
{
    public $invoice;
    public $invoice_number;
    public $amount;
    public $customer_id;
    public $status;
    public $name;
    public $number;


    public static function tableName()
    {
        return 'intouch_payments';
    }

    public function rules()
    {
        return [
            [['touchpay_id', 'status'], 'required'],
            [['customer_id', 'touchpay_id', 'service'], 'string', 'max' => 40],
            [['amount', 'fees'], 'double', 'min' => 0],
            [['customer_phone', 'status'], 'string', 'max' => 10]

        ];

    }

    public function attributeLabels()
    {
        return[
            'trans_id' => 'Trans ID',
            'customer_id' => 'Customer ID',
            'touchpay_id' => 'Touchpay ID',
            'amount' => 'Amount',
            'fees' => 'Fees',
            'service' => 'Service',
            'customer_phone' => 'Customer Phone',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
        ];
    }

    
    public function getAddonTitle()
    {
        return "Touchpay Payments";
    }


    public function getCustomer()
    {
        
        return (new BaseCustomer())->findById($this->customer_id);

    }

    public function getInvoiceNumber()
    {
        return $this->number;
    }


    public function getInvoiceAmount()
    {
        return $this->amount;

    }

    public function getInvoiceStatus()
    {
        return $this->status;

    }

    
    public function initiatePayment()
    {
        function callAPI($method, $url, $data){
            $curl = curl_init();
            $login='MTN:passer';
            switch ($method){
             case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                 break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                 if ($data)
                     curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
        default:
                 if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

            // OPTIONS:
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'APIKEY: 111111111111111111111',
                'Content-Type: application/json',
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $login);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);



            // EXECUTE:
            $result = curl_exec($curl);
            if(!$result){die("Connection Failure");}
            curl_close($curl);
            return $result;
    }

            $url = "https://dev-api.gutouch.com/dist/api/touchpayapi/v1/INTDK0794/transaction?loginAgent=774338370&passwordAgent=0000";
            $client_id = $this->number; 
            $amount = $this->amount;
            // customer info
            $customer = $this->getCustomer();
            $email = $customer->email;
            $firstname = $customer->name;
            $lastname = "-ARC";
            $phone = $customer->phone;
            $service_code = "PAIEMENTMARCHANDOM";


        $data_array = array(
            "idFromClient" => $client_id,

            "additionnalInfos" => array(
                "recipientEmail" => $email,
                "recipientFirstName"=> $firstname,
                "recipientLastName" => $lastname,
                "destinataire" => $phone //phone number without country code
            ),

            "amount" => $amount,
            "callback" => "https://emarchand-test.appspot.com/callbackApi", // Touchpay sandbox
            //"callback" => "http://894ffe86.ngrok.io/TouchpayRAW/calltest3.php",
            "recipientNumber" => $phone, // same as destinataire
            "serviceCode" => $service_code // service type
        );

        $params = json_encode($data_array);
        $initiate_payment = callAPI('PUT', $url, $params);

        $response = json_decode($initiate_payment, true);
       // print_r($response);

   
            if (!empty($response)) {
                $client_id = $response['idFromClient'];
                $touchpay_id = $response['idFromGU'];
                $amount = $response['amount'];
                $fees = $response['fees'];
                $service = $response['serviceCode'];
                $customer_phone = $response['recipientNumber'];
                $date_time = $response['dateTime'];
                $status = $response['status'];
            }

           echo "Thankyou! transaction" .$touchpay_id. "has been "  .$status. "kindly finish the payment on your phone.";

           // my local Database Test
        /*   
        $servername = "localhost";
        $username = "admin";
        $password = "starwars";
        $db = "splynx";

        $con = mysqli_connect($servername, $username, $password, $db);

        if (!$con)
        {
        die("Connection Failed: " . mysqli_connect_error());
        }

        // save first response
        $sql = "INSERT INTO touchpay (customer_id, touchpay_id, amount, fees, service, customer_phone, timestamp, status)
        VALUE ('$client_id', '$touchpay_id', '$amount', '$fees', '$service', '$customer_phone', '$date_time', '$status')";


        if (!mysqli_query($con,$sql)) {
        echo mysqli_error($con);
        }
        
        */
        

    }


}



?>