<?php

use yii\db\Migration;

/**
 * Class m190816_080906_create_intouch_payments
 */
class m190816_080906_create_intouch_payments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('intouch_payments',[
            'trans_id' => $this->primaryKey(),
            'customer_id' => $this->string(40),
            'touchpay_id' => $this->string(40),
            'amount' => $this->float(),
            'fee' => $this->float(),
            'service' => $this->string(40),
            'customer_phone' => $this->string(10),
            'timestamp' => $this->time(),
            'status' => $this->string(10),

        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('intouch_payments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190816_080906_create_intouch_payments cannot be reverted.\n";

        return false;
    }
    */
}
